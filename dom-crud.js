// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
let anchorTag = document.createElement("a")
anchorTag.innerText = "Buy Now!"


anchorTag.setAttribute("id", "cta")
// console.log(anchorTag);

let mainTag = document.getElementsByTagName("main")[0]

// console.log(mainTag);

mainTag.appendChild(anchorTag);

// Access (read) the data-color attribute of the <img>,
// log to the console 

let imageRef = document.getElementsByTagName("img")[0];
let data = imageRef.dataset.color;
// let data = imageRef.getAttriute("data-color")
console.log(data);


// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"

const thirdList = document.getElementsByTagName("ul")[0];
thirdList.querySelector(':nth-child(3)').className = "highlight";
// thirdList.setAtrribut("Turbocharged", "highlight");
console.log(thirdList);

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")

let lastPara = document.querySelector("main > p")
console.log(lastPara);

mainTag.removeChild(lastPara)


